# Tools for Attact & Defence CTFs

## How to run tools

* Configure DestructionFarm in the `DestructiveFarm_config.py` file.
* Checkout `DestructiveFarm_database.sqlite` file before starting new CTF.
* Run `docker-compose up --build --force-recreate -d`
* `caronte` has to be configured on the first run

## How to check UI

* Add information about services to `/etc/hosts`
```
127.0.0.1   destructive-farm caronte
```
* Open browser and go to [destructive-farm](http://destructive-farm/) or [caronte](http://caronte/)

## How to run new exploits

* Save your working exploit in the `exploits` directory - you can based on `exploits/example.py` (last and 1st lines are important)
* Run command: `docker-compose exec -it destructive-farm /tools/start_sploit.py /tools/exploits/example.py -u http://127.0.0.1:5000 -a EXPLOIT_NAME`
* For HITB CTF: `docker-compose exec -it destructive-farm /tools/run_sploit.py /tools/exploits/example.py -u http://127.0.0.1:5000 -a EXPLOIT_NAME -s SERVICE_NAME`

## How to stop tools

`docker-compose down`